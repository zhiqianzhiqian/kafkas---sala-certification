```properties
# 1.解压
tar -xzvf /home/econ/zookeeper-3.5.9-bin.tar.gz 
# 2.重命名
mv /home/econ/apache-zookeeper-3.5.9-bin /home/econ/zk
# 3.复制配置文件
cp /home/econ/zk/conf/zoo_sample.cfg /home/econ/zk/conf/zoo.cfg
# 4.配置文件内容
tickTime=2000
initLimit=10
syncLimit=5
dataDir=/home/econ/zk/data
dataLogDir=/home/econ/zk/log
clientPort=2181
# 5.启动
/home/econ/zk/bin/zkServer.sh start
# 6.查看状态
/home/econ/zk/bin/zkServer.sh status
```

安装kafka

```properties
#1. 解压
tar -zxf /home/econ/kafka_2.12-2.5.1.tgz
#2. 重命名
mv /home/econ/kafka_2.12-2.5.1 /home/econ/kafka
# 3.修改config文件  /home/econ/kafka/config/server.properties
broker.id=0
num.network.threads=3
num.io.threads=8
socket.receive.buffer.bytes=102400
socket.request.max.bytes=104857600
log.dirs=/home/econ/kafka/logs
num.partitions=1
offsets.topic.replication.factor=1
transaction.state.log.replication.factor=1
transaction.state.log.min.isr=1
log.retention.hours=168
log.segment.bytes=1073741824
log.retention.check.interval.ms=300000
zookeeper.connect=localhost:2181
zookeeper.connection.timeout.ms=18000
group.initial.rebalance.delay.ms=0
# 4.创建数据日志目录
mkdir /home/econ/kafka/logs
# 5.启动kafka
/home/econ/kafka/bin/kafka-server-start.sh -daemon /home/econ/kafka/config/server.properties
# 6.查看启动结果
tail -100f /home/econ/kafka/logs/server.log
```

生产者消费者

```properties
#创建topic
/home/econ/kafka/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test
#创建生产者
/home/econ/kafka/bin/kafka-console-producer.sh --broker-list localhost:9092 --topic test
#创建消费者
/home/econ/kafka/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic test --from-beginning
#删除主题
/home/econ/kafka/bin/kafka-topics.sh --delete --zookeeper localhost:2181 --topic test2
#查看描述
/home/econ/kafka/bin/kafka-topics.sh --describe --zookeeper localhost:2181 --topic test
```

### zookeeper的安全认证配置

1、导入kafka的相关jar

从kafka/lib目录下复制以下几个jar包到zookeeper的lib目录下：

```properties
kafka-clients-2.5.1.jar
lz4-java-1.7.1.jar
slf4j-api-1.7.30.jar
slf4j-log4j12-1.7.30.jar
snappy-java-1.1.7.3.jar
```

2、zoo.cfg文件配置
添加如下配置：

```properties
authProvider.1=org.apache.zookeeper.server.auth.SASLAuthenticationProvider
requireClientAuthScheme=sasl
jaasLoginRenew=3600000
```

3、编写JAAS文件，zk_server_jaas.conf，放置在conf目录下
这个文件定义需要链接到Zookeeper服务器的用户名和密码。JAAS配置节默认为Server：

```properties
Server {
		org.apache.kafka.common.security.plain.PlainLoginModule required
    username="admin"
    password="admin"
    user_admin="admin"
    user_kafka="kafka-2022";
};

```

这个文件中定义了两个用户，一个是admin，一个是kafka，这些用user_配置出来的用户都可以提供给生产者程序和消费者程序认证使用。还有两个属性，username和password，其中username是配置Zookeeper节点之间内部认证的用户名，password是对应的密码。
4、[修改zkEnv.sh](http://xn--zkenv-3u3h158j.sh/)
在zkEnv.sh添加以下内容，路径按你直接的实际路径来填写：

```properties
export SERVER_JVMFLAGS=" -Djava.security.auth.login.config=/home/econ/zk/conf/zk_server_jaas.conf "
```

5、重启zookeeper

```properties
# 1.查看进程
ps aux | grep zk
# 2.杀进程
kill 15425
# 3.启动zookeeper
/home/econ/zk/bin/zkServer.sh start
#启动失败得话可以将dataDir=/home/econ/zk/data里面的数据删除再执行启动命令
```

![image-20220114162734969](E:\soft-dev\typora\pic-data\image-20220114162734969.png)

如图所示则表示启动成功

### kafka的安全认证配置

1、在kafka的config目录下，新建kafka_server_jaas.conf文件，内容如下：

```properties
KafkaServer {
	org.apache.kafka.common.security.plain.PlainLoginModule required
	username="admin"
	password="admin"
	user_admin="admin"
	user_producer="prod-2022"
	user_consumer="cons-2022";
};

Client {
org.apache.kafka.common.security.plain.PlainLoginModule required
	username="admin"
	password="admin";
};
```

KafkaServer配置的是kafka的账号和密码，Client配置节主要配置了broker到Zookeeper的链接用户名密码，这里要和前面zookeeper配置中的zk_server_jaas.conf中user_admin的账号和密码相同。

2、配置server.properties，同样的在config目录下

```properties
listeners=SASL_PLAINTEXT://0.0.0.0:9092
advertised.listeners=SASL_PLAINTEXT://192.168.86.221:9092
security.inter.broker.protocol=SASL_PLAINTEXT  
sasl.enabled.mechanisms=PLAIN  
sasl.mechanism.inter.broker.protocol=PLAIN  
authorizer.class.name=kafka.security.auth.SimpleAclAuthorizer
allow.everyone.if.no.acl.found=true
```

这里注意listeners配置项，将主机名部分（本例主机名是192.168.86.221）替换成当前节点的主机名。其他在各个节点的配置一致。注意，allow.everyone.if.no.acl.found这个配置项默认是false，若不配置成true，后续生产者、消费者无法正常使用Kafka。

3、在server启动脚本JVM参数,在bin目录下的kafka-server-start.sh中，将

```properties
export KAFKA_HEAP_OPTS="-Xmx1G -Xms1G"
```

这一行修改为

```properties
export KAFKA_HEAP_OPTS="-Xmx1G -Xms1G -Djava.security.auth.login.config=/home/econ/kafka/config/kafka_server_jaas.conf"
```

4、启动kafka

```properties
/home/econ/kafka/bin/kafka-server-start.sh -daemon /home/econ/kafka/config/server.properties
```

- 可能会出现`org.apache.zookeeper.server.persistence.FileTxnSnapLog$SnapDirContentCheckException: Snapshot directory has log files. Check if dataLogDir and dataDir configuration is correct.`的异常
  - kafka配置日志路径，用来保存执行过程中的各种信息，当kafka异常关闭时，日志记录就会出现异常，会把当时的情况记录到meta.properties文件中，重新启动时此文件会对启动造成影响，kafka重启报错的原因就在这里。
  - 解决方案
    - 清空日志目录【日志不重要或可以容忍日志丢失】。
    - 调整日志目录【需要修改配置】。
    - 删除日志目录下的meta.properties文件。

![image-20220114171212100](E:\soft-dev\typora\pic-data\image-20220114171212100.png)

