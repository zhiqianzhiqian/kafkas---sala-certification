package com.econ;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @ Description:
 * @ Author：hgp
 * @ Create:2022-01-13-14:37
 */
@RestController
public class Controller {
    @Autowired
    private Sender sender;

    @GetMapping("/send")
    public String send(@RequestParam String msg) {
        sender.send(msg);
        return msg;
    }
}
