package com.econ;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * @ Description:
 * @ Author：hgp
 * @ Create:2022-01-13-14:36
 */
@Component
public class Receiver {
    @KafkaListener(topics = "probe2")
    public void receiveMessage(ConsumerRecord<String, String> record) {
        System.out.println("消费者开始接收消息  key = " + record.key() + "、value = " + record.value());
        //TODO，在这里进行自己的业务操作，例如入库
    }
}
